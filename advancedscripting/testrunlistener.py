from de.qfs.apps.qftest.extensions.qftest import TestRunListener
from de.qfs.apps.qftest.shared.extensions import RunContext

import qfcommon

global rc

#Get run context from QF-Test
rc = qfcommon.globalDict['__rc']

class Testrunlistener(TestRunListener):
    def __init__(self):
        pass

    def runStarted(self, event):
        print "Run started"

    def runStopped(self, event):
        print "Run stopped"

    def nodeEntered(self, event):
        name = event.getNode().getName()
        nodeType = event.getNode().getType()
        if nodeType == "TestCase":
            if name:
                parts = name.split(":")
                rc.setGlobal("tcid", parts[0])


    def nodeExited(self, event):
        pass

    def problemOccurred(self, event):
        pass

    
global listener

try:
    RunContext.removeGlobalTestRunListener(listener)
except:
    pass

listener = Testrunlistener()
RunContext.addGlobalTestRunListener(listener)
