#!/bin/sh

if [ -n ${TZ} ]; then
    ln -nfs "/usr/share/zoneinfo/${TZ}" /etc/localtime

    #this sets /etc/timezone which is only a debian specific
    dpkg-reconfigure -f noninteractive tzdata
fi

exec ${MAINPROCESS:-/qftest/qftest-container/bin/qftest} "$@"
