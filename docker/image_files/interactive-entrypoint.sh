#!/bin/bash

# creating symlink for setting correct timezone based on TZ env
if [ -n ${TZ} ]; then
    ln -nfs "/usr/share/zoneinfo/${TZ}" /etc/localtime

    #this sets /etc/timezone which is only a debian specific
    dpkg-reconfigure -f noninteractive tzdata
fi

export DISPLAY=:1

teardown() {
	echo Tearing down...
	if [ -n "$pid_qft" ]; then
		kill -TERM "$pid_qft"
	fi
	if [ -n "$pid_wm" ]; then
		kill -TERM "$pid_wm"
	fi
	if [ -n "$pid_vncconfig" ]; then
		kill -TERM "$pid_vncconfig"
	fi
	if [ -n "$pid_vnc" ]; then
		kill -TERM "$pid_vnc"
	fi
}

trap teardown SIGINT SIGTERM EXIT

echo "${VNCPASSWD:-"qf-test"}" | vncpasswd -f > "${HOME}/.vncpasswd"
chmod 600 "${HOME}/.vncpasswd"

/usr/bin/Xtigervnc ${DISPLAY} -geometry ${GEOMETRY:-"1680x1050"} -desktop QF-Test -rfbauth "${HOME}/.vncpasswd" -rfbport 5901 2>>/vnc.err >>/vnc.log &
pid_vnc="$!"

while ! xdpyinfo > /dev/null 2>&1; do
    echo Waiting for X server...
    sleep 0.1
done


openbox-session 2>>/openbox.err >>/openbox.log &
pid_wm="$!"

while ! wmctrl -m >/dev/null 2>&1; do
    echo Waiting for desktop session...
    sleep 0.1
done


if [[ ! " $@ " =~ " -batch " ]]; then
	vncconfig -iconic &
	pid_vncconfig="$!"
fi

hsetroot -solid "#656565"
${MAINPROCESS:-/qftest/qftest-container/bin/qftest} "$@" &
pid_qft="$!"

wait "$pid_qft"
ret=$?
pid_qft=

exit $ret
