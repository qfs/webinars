<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE RootStep>
<RootStep id="_0" name="root"
          signature="302D0215009A5028423573091C0DB790A5A0562AB9E7A7C1D502142F312048CBA517C80E11CF81D09B59996C43FFEF"
          version="5.4.1">
  <include>qfs.qft</include>
  <include>components.qft</include>
  <variable name="buggyMode">False</variable>
  <comment>This test-suite contains a number of sample scenarios and workflows for the Swing-based car configuration demo application that is also used and described in the advanced tutorial. Basic and advanced QF-Test techniques are used like test-sets, test-cases, dependencies, procedures, data-drivers, nested loops, try/catch.

@param	buggyMode    If you set this variable to true some test-cases will fail, so you can check the error reporting of QF-Test.

@author QFS</comment>
  <PackageRoot id="_31">
    <Package id="_3Cr" name="dependencies">
      <comment>Package for dependencies.</comment>
      <Dependency id="_3Cs" name="sutStarted">
        <comment>Ensure the SUT is started.</comment>
        <SetupSequence id="_3Ac" name="Start SUT, if required">
          <SetGlobalStep id="_1p6F" varname="client">
            <default>carconfig</default>
          </SetGlobalStep>
          <ClientWaiter client="$(client)" id="_1p6G" local="true"
                        raise="false" resvarname="isSUTRunning" timeout="1">
            <comment>This node checks whether the SUT is already running. The result of this check will be stored in the variable isSUTRunning. The variable itself can contain true if SUT is already running or false if SUT is not running. This variable will be used in the following 'If' node.</comment>
          </ClientWaiter>
          <IfSequence id="_1p6H" name="Launch SUT if not running"
                      test="not $(isSUTRunning)">
            <ProcedureCall id="_3Ad" procedure="startStop.start"/>
          </IfSequence>
        </SetupSequence>
        <CleanupSequence id="_3Cm" name="Close SUT">
          <ProcedureCall id="_3D0" procedure="startStop.terminate"/>
        </CleanupSequence>
      </Dependency>
      <Dependency forcedcleanup="true" id="_3Cw" name="sutStartedAndReset">
        <comment>Ensure proper state of running SUT.
The buggy mode will be set depending on the global variable 'buggyMode'.</comment>
        <DependencyReference id="_3DA" reference="dependencies.sutStarted"/>
        <SetupSequence id="_3Aj" name="Reset">
          <ProcedureCall id="_3Ak" procedure="mainwindow.menu.reset"/>
          <ProcedureCall id="_1p6-" procedure="mainwindow.menu.setBuggy">
            <variable name="isBuggy">$(buggyMode)</variable>
          </ProcedureCall>
        </SetupSequence>
      </Dependency>
      <Dependency forcedcleanup="true" id="_3HG"
                  name="sutStartedAndResetWithCustomCheck">
        <comment>Register a custom check.</comment>
        <DependencyReference id="_3HH"
                             reference="dependencies.sutStartedAndReset"/>
        <SetupSequence id="_3H2" name="Register check">
          <ClientScriptStep client="$(client)" id="_3AM"
                            name="registering a custom check">
            <code>from de.qfs.apps.qftest.extensions import ResolverRegistry
from de.qfs.apps.qftest.extensions.checks import CheckerRegistry, Checker, DefaultCheckType, CheckDataType
from de.qfs.apps.qftest.shared.data.check import StringCheckData
from de.qfs.lib.util import Pair
from java.lang import String
import jarray

# Custom check type
currencyTextCheckedCheckType = DefaultCheckType(
    "CurrencyText_checked",            # Internal name
    CheckDataType.STRING,         # Type of check
    "Text without currency"   # Text for Popup-Menu
)

#helper method for delivering the pure number and remove any letters
def getTextWithoutCurrency(text):
    return String(text).replaceAll("[^0-9|.|,|']", "")
    
class MyCurrencyChecker(Checker):
    # Supported check types
    def getSupportedCheckTypes(self, com, item):
        return jarray.array([currencyTextCheckedCheckType], DefaultCheckType)

    # Data for checking during replay and during recording
    def getCheckData(self, com, item, checkType):
        if currencyTextCheckedCheckType.getIdentifier() == checkType.getIdentifier():
            text = getTextWithoutCurrency(com.getText())        
            return StringCheckData(checkType.getIdentifier(), text)
        return None

    # Data during recording
    def getCheckDataAndItem(self, com, item, checkType):
        data = self.getCheckData(com, item, checkType)
        if data is None:
            return None
        return Pair(data, None)   

def unregister():
    try:
        CheckerRegistry.instance().unregisterChecker("javax.swing.JTextField", myCurrencyChecker)
    except:
        pass

def register():
    global myCurrencyChecker
    unregister()
    myCurrencyChecker = MyCurrencyChecker()
    CheckerRegistry.instance().registerChecker("javax.swing.JTextField", myCurrencyChecker)

register()

</code>
          </ClientScriptStep>
        </SetupSequence>
      </Dependency>
      <Dependency forcedcleanup="true" id="_3Jv"
                  name="sutStartedAndResetWithCustomAlgorithmCheck">
        <comment>Register a custom check.</comment>
        <DependencyReference id="_3Jw"
                             reference="dependencies.sutStartedAndReset"/>
        <SetupSequence id="_3Jx" name="Register check">
          <ClientScriptStep client="$(client)" id="_3Jz"
                            name="registering a custom check">
            <code>from de.qfs.apps.qftest.extensions import ResolverRegistry
from de.qfs.apps.qftest.extensions.checks import CheckerRegistry, Checker, DefaultCheckType, CheckDataType
from de.qfs.apps.qftest.shared.data.check import ImageCheckData
from imagewrapper import ImageWrapper
from de.qfs.lib.util import Pair
import jarray

# Custom check type
algorithmCheckType = DefaultCheckType(
    "algorithm",            # Internal name
    CheckDataType.IMAGE,         # Type of check
    "Image with algorithm"   # Text for Popup-Menu
)

    
class MyAlgorithmChecker(Checker):
    # Supported check types
    def getSupportedCheckTypes(self, com, item):
        return jarray.array([algorithmCheckType], DefaultCheckType)

    # Data for checking during replay and during recording
    def getCheckData(self, com, item, checkType):
        if algorithmCheckType.getIdentifier() == checkType.getIdentifier():
            iw = ImageWrapper(rc)
            image = iw.grabImage(com)
            icd = ImageCheckData(checkType.getIdentifier(), image, -1, -1, -1, -1, -1, -1)
            icd.setAlgorithm("algorithm=similarity;expected=0.97")
            return icd
        return None

    # Data during recording
    def getCheckDataAndItem(self, com, item, checkType):
        data = self.getCheckData(com, item, checkType)
        if data is None:
            return None
        return Pair(data, None)   

def unregister():
    try:
        CheckerRegistry.instance().unregisterChecker("javax.swing.JComponent", myAlgorithmChecker)
    except:
        pass

def register():
    global myAlgorithmChecker
    unregister()
    myAlgorithmChecker = MyAlgorithmChecker()
    CheckerRegistry.instance().registerChecker("javax.swing.JComponent", myAlgorithmChecker)

register()

</code>
          </ClientScriptStep>
        </SetupSequence>
      </Dependency>
    </Package>
    <Package id="_3Fr" name="startStop">
      <comment>Package for starting and stopping the SUT.</comment>
      <Procedure id="_33x" name="start">
        <comment>Start the SUT.</comment>
        <IfSequence id="_33z" test="${qftest:windows}">
          <SUTClientStarter client="$(client)"
                            directory="${qftest:dir.version}/demo/carconfig"
                            executable="${qftest:dir.version}/demo/carconfig/CarConfig.cmd"
                            id="_33+">
            <parameter>en</parameter>
          </SUTClientStarter>
          <ElseSequence id="_33-">
            <SUTClientStarter client="$(client)"
                              directory="${qftest:dir.version}/demo/carconfig"
                              executable="${qftest:dir.version}/demo/carconfig/CarConfig.sh"
                              id="_3E2">
              <parameter>en</parameter>
            </SUTClientStarter>
          </ElseSequence>
        </IfSequence>
        <ClientWaiter client="$(client)" id="_E" timeout="30000"/>
        <ComponentWaiter client="$(client)" component="JCarConfigurator"
                         id="_3Fm" timeout="30000"/>
      </Procedure>
      <Procedure id="_3C-" name="terminate">
        <comment>Terminate the SUT.</comment>
        <TryStep id="_3DH" name="SUT running?">
          <ClientWaiter client="$(client)" id="_3DI" timeout="500"/>
          <ProcedureCall id="_3D2"
                         procedure="qfs.cleanup.implicitExceptionHandler"/>
          <MouseEventStep clicks="1" client="$(client)" component="mFile"
                          event="MOUSE_MPRC" id="_3Cn" modifiers="16"/>
          <MouseEventStep clicks="1" client="$(client)" component="miExit"
                          event="MOUSE_MPRC" id="_3Co" modifiers="16"/>
          <TryStep id="_3G8" name="wait for termination">
            <ProcessWaiter client="$(client)" id="_3Cp"/>
            <CatchSequence exception="TestException" id="_3G9" maxerror="0"
                           name="SUT not terminated">
              <ClientStopper client="$(client)" id="_3Cq"/>
              <ProcessWaiter client="$(client)" id="_3GA"/>
            </CatchSequence>
          </TryStep>
          <CatchSequence exception="ClientNotConnectedException" id="_3DJ"
                         maxerror="0"/>
        </TryStep>
      </Procedure>
    </Package>
  </PackageRoot>
  <ExtraSequence id="_5y"/>
  <WindowList id="_61"/>
</RootStep>
